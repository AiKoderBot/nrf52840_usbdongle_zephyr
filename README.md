# nrf52840_usbdongle_zephyr

## how to update app via `nrfutil`
```
# create *.zip file
./nrfutil nrf5sdk-tools pkg generate --hw-version 52 --sd-req=0x00 --application build/zephyr/zephyr.hex --application-version 1 blinky.zip

# update via usb-serial
./nrfutil nrf5sdk-tools dfu usb-serial -pkg blinky.zip -p COM25
```
